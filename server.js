//LIBRERIA EXPRESS
var express = require('express');
var app = express();
var bodyParser = require('body-parser');
app.use(bodyParser.json());
var port=process.env.PORT || 3000;
var requestJson = require('request-json');
var baseMlabURL="https://api.mlab.com/api/1/databases/apitechujco/collections/";
var mLabAPIKey="apiKey=OAcuJLU6YBy12KCVHbRGtFdGcEKpsqKG";

app.listen(port);
console.log("API escuchando en el puerto bip bip "+port)

app.get('/apitechu/v1',
 function(req,res){
   console.log("GET /apitechu/v1");
   res.send({
    "msg" :"Bienvenido a la API Tech University"
  });
 }
);

app.get('/apitechu/v1/users',
function(req,res){
  console.log("GET /apitechu/v1/users");
  res.sendFile('usuarios.json',{root: __dirname});
}
);

app.post('/apitechu/v1/users',
function(req,res){
  console.log("POST /apitechu/v1/users");
  console.log("first name "+req.headers.first_name);
  console.log("last name "+ req.headers.last_name);
  console.log("country "+ req.headers.country);
  var newUser ={
    "first_name" : req.headers.first_name,
    "last_name" : req.headers.last_name,
    "country" : req.headers.country
  };

 var users = require('./usuarios.json');
 users.push(newUser);
 writeUserDataToFile(users);
res.send({ "msg" :"FIn del POST de la API Tech University"});
}
);

app.delete('/apitechu/v1/users/:id',
  function(req,res){
    console.log("DELETE /apitchu/v1/users/:id");
    var users = require('./usuarios.json');
    users.splice(req.params.id-1,1);
    writeUserDataToFile(users);
  res.send({"msg" :"Usuario borrado"});
}
);


function writeUserDataToFile(data){
var fs=require('fs');
var jsonUserData= JSON.stringify(data);
fs.writeFile("./usuarios.json",jsonUserData,"utf8",function(err){
 if (err){
    console.log(err);
  }
  else {
    console.log("Success en fichero");
  }
});
}

app.post('/apitechu/v1/monstruo/:p1/:p2',
  function(req,res){
/*
    console.log("Parametros");
    console.log(req.params);
    console.log("Query String");
    console.log(req.query);
    console.log("Headers");
    console.log(req.headers);*/

    console.log("first name "+req.body.first_name);
    console.log("last name "+ req.body.last_name);
    console.log("country "+ req.body.country);

    var newUser ={
      "first_name" : req.body.first_name,
      "last_name" : req.body.last_name,
      "country" : req.body.country
    };

    var users = require('./usuarios.json');
    users.push(newUser);
    writeUserDataToFile(users);

    res.send({"msg" :"FIN monstruo"});
  }
);

app.post('/apitechu/v1/login',
  function(req,res){

    var logU ={
    "email" : req.body.email,
    "password" : req.body.password
    };
 var resultado=LogUser(logU,"./login.json");
   if(resultado>0){
     res.send({"msg" :"Usuario logueado OK",
                "idUsuario" : resultado});
   }
   if(resultado==-1){
     res.send({"msg" :"Email NO ENCONTRADO"});
   }
   if(resultado==0){
     res.send({"msg" :"Password Usuario incorrecto"});
   }

  }
);

function LogUser(loguser,data){
var users = require(data);
var login =-1;
for (user of users) {
      if(loguser.email==user.email&&loguser.password!=user.password){
        login=0;
        break;
      }
      if(loguser.email==user.email&&loguser.password==user.password){
        login=user.id;
        loguser.logged=true;
        users.push(loguser);
        writeUserDataToFile(users);
        break;
      }
   }
   console.log("salimos ddel bucle "+login);
   return login;
 }


 app.post('/apitechu/v1/logout',
 function(req,res){

 id=req.body.id;

 var resultado=Logout(id,"./login.json");
  if(resultado==0){
    res.send({"msg" :"Usuario logout OK",
               "idUsuario" : id});
  }else{
    res.send({"msg" :"Usuario logout ERR"});
  }
}
 );

 function Logout(loguser,data){
 var users = require(data);
 var logout =-1;
 for (user of users) {
       if(user.id==loguser&&user.logged==true){
         delete user.logged;
         logout=0;
         users.push(user);
         writeUserDataToFile(users);
         break;
       }
    }
console.log(logout);
    return logout;
  }

app.get('/apitechu/v2/users',
  function(req,res){
    console.log("GET /apitechu/v2/users");
    httpClient=requestJson.createClient(baseMlabURL);
    console.log("Cliente creado");
    httpClient.get("user?"+mLabAPIKey,
      function(err, resMLab, body){
        var response = !err ? body : {
          "msg" : "Error obteniendo usuarios."
        }
        res.send(response);
      }

    );
  }
);

app.get('/apitechu/v2/users/:id',
 function(req, res) {
   console.log("GET /apitechu/v2/users/:id");

   var id = req.params.id;
   var query = 'q={"id" : ' + id + '}';

   httpClient = requestJson.createClient(baseMlabURL);
   console.log("Cliente creado");

   httpClient.get("user?" + query + "&" + mLabAPIKey,
     function(err, resMLab, body) {
       if (err) {
         response = {
           "msg" : "Error obteniendo usuario."
         }
         res.status(500);
       } else {
         if (body.length > 0) {
           response = body[0];
         } else {
           response = {
             "msg" : "Usuario no encontrado."
           };
           res.status(404);
         }
       }
       res.send(response);
     }
   )
 }
);

//login usuario en BBDD
app.post('/apitechu/v2/login',
 function(req, res) {
   console.log("POST /apitechu/v2/login");

  var querybd = 'q={"email" : "' + req.body.email + '" ,"password":"'+ req.body.password +'"}';
 httpClient = requestJson.createClient(baseMlabURL);

   httpClient.get("user?" + querybd + "&" + mLabAPIKey,
     function(err, resMLab, body) {
       if (err) {
         response = {
           "msg" : "Error obteniendo usuario."
         }
         res.send(response);
         res.status(500);
       } else {
         if (body.length > 0) {
           //response = body[0];
           var querybody = 'q={"id" : ' + body[0].id +'}';
           var putBody='{"$set":{"logged":true}}';
           httpClient.put("user?"+ querybody+"&"+mLabAPIKey, JSON.parse(putBody),
              function(errPUT,resMlabPUT,bodyPUT){
                  response = { "msg" : "Usuario LOGGIN OK",
                  "idUsuario" : body[0].id};
                  res.send(response);
              }
            );
         } else {
           response = {"msg" : "Usuario no encontrado."};
           res.status(404);
           res.send(response);

       }
       }
    }
   )
 }
);

app.post('/apitechu/v2/logout/:id',
 function(req, res) {
   console.log("POST /apitechu/v2/logout/:id");

   var query = 'q={"id": ' + req.params.id + '}';
   console.log("query es " + query);

   httpClient = requestJson.createClient(baseMlabURL);
   httpClient.get("user?" + query + "&" + mLabAPIKey,
     function(err, resMLab, body) {
       if (body.length == 0) {
         var response = {
           "mensaje" : "Logout incorrecto, usuario no encontrado"
         }
         res.send(response);
       } else {
         console.log("Got a user with that id, logging out");
         query = 'q={"id" : ' + body[0].id +'}';
         console.log("Query for put is " + query);
         var putBody = '{"$unset":{"logged":""}}'
         httpClient.put("user?" + query + "&" + mLabAPIKey, JSON.parse(putBody),
           function(errPUT, resMLabPUT, bodyPUT) {
             console.log("PUT done");
             var response = {
               "msg" : "Usuario deslogado con éxito",
               "idUsuario" : body[0].id
             }
             res.send(response);
           }
         )
       }
     }
   );
 }
);


app.get('/apitechu/v2/users/:id/accounts',
 function(req, res) {
   console.log("GET /apitechu/v2/users/:id/accounts");

   var id = req.params.id;
   var query = 'q={"id" : ' + id + '}';

   httpClient = requestJson.createClient(baseMlabURL);

   httpClient.get("account?" + query + "&" + mLabAPIKey,
     function(err, resMLab, body) {
       if (err) {
         response = {
           "msg" : "Error obteniendo cuenta de usuario."
         }
         res.status(500);
       } else {
         if (body.length > 0) {
           response = body;
         } else {
           response = {
             "msg" : "Cuenta no encontrada."
           };
           res.status(404);
         }
       }
       res.send(response);
     }
   )
 }
);
