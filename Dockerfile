# imagen raiz
FROM node

#carpeta raiz
WORKDIR /apitechu

#copia de archivos
ADD . /apitechu

#Añadir volumen
VOLUME ['/logs']

#Exponer puerto
EXPOSE 3000

#instalar dependencias
#RUN npm install

# cOMANDO DE INICIALIZACION
CMD ["npm", "start"]
